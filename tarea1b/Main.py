import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import sys
from screeninfo import get_monitors
import basic_shapes as bs
import scene_graph as sg
import easy_shaders as es
import time

# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES = 4

# width = get_monitors()[0].width
# height = get_monitors()[0].height
width = 900
height = 720


# A class to store the application control
class Controller:
    def __init__(self):
        #Luces auto
        self.lights = False
        # Street
        self.si = -2.0
        self.sc = 0.0
        self.sd = 2.0
        # SandBack
        self.sandi = -2.0
        self.sandc = 0.0
        self.sandd = 2.0
        # SandBack2
        self.sandi2 = -2.0
        self.sandc2 = 0.0
        self.sandd2 = 2.0
        # Sea
        self.seai = -2.0
        self.seac = 0.0
        self.sead = 2.0
        # Sky
        self.skyi = -2.0
        self.skyc = 0.0
        self.skyd = 2.0
        # SandFront
        self.sandfi = -2.0
        self.sandfc = 0.0
        self.sandfd = 2.0
        # SandFront2
        self.sandfi2 = -2.0
        self.sandfc2 = 0.0
        self.sandfd2 = 2.0
        # SandFront3
        self.sandfi3 = -2.0
        self.sandfc3 = 0.0
        self.sandfd3 = 2.0

        # Velocidad de auto
        self.velocidad = 0
        self.velocidadmax = 0.02
        self.velocidadmaxbackup = 0.02
        self.velocidadmin = 0.005
        self.avance = 0.0004
        self.avancebackup = 0.0004
        self.retroceso = 0.0002
        self.moviendose = False
        self.derecha = False
        self.izquierda = False

        #Rotation
        self.rotation=0.0
        #Movimientos automaticos
        self.boat=0.0
        self.boatvel=0.0005
        self.sun=0.00
        self.sunvel=0.0001
        self.nube=-0.6
        self.nube2 = -0.4
        self.nube3 = 0.9
        self.nubevel=0.0003
        #Boost
        self.boost = False
        self.boostmax = 0.1
        #Aesthethic
        self.aesthetic=False




controller = Controller()


def on_key(window, key, scancode, action, mods):
    global controller
    if action != glfw.PRESS and action != glfw.REPEAT:
        controller.moviendose = False

    # Keep pressed buttons
    if action == glfw.PRESS and key == glfw.KEY_A:
        controller.moviendose = True

        if (controller.velocidad >= 0):
            controller.izquierda = True
            controller.derecha = False
        if controller.velocidad >= controller.velocidadmin:
            controller.velocidad = controller.velocidadmin
        else:
            controller.velocidad += controller.retroceso



    elif action == glfw.PRESS and key == glfw.KEY_D:
        controller.moviendose = True

        if (controller.velocidad <= 0):
            controller.derecha = True
            controller.izquierda = False
        if controller.velocidad <= -controller.velocidadmax:
            controller.velocidad = -controller.velocidadmax
        else:
            controller.velocidad -= controller.avance



    elif key == glfw.KEY_A:

        if (controller.moviendose == True):
            if (controller.velocidad >= 0):
                controller.izquierda = True
                controller.derecha = False
            if controller.velocidad >= controller.velocidadmin:
                controller.velocidad = controller.velocidadmin
            else:
                controller.velocidad += controller.retroceso


    elif key == glfw.KEY_D:
        if (controller.moviendose == True):
            if (controller.velocidad <= 0):
                controller.derecha = True
                controller.izquierda = False
            if controller.velocidad <= -controller.velocidadmax:
                    controller.velocidad = -controller.velocidadmax
            else:
                     controller.velocidad -= controller.avance

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

    elif key == glfw.KEY_2 and action == glfw.PRESS:
         controller.boost = not controller.boost
         if controller.boost==True:

            controller.velocidadmax=controller.boostmax
            controller.avance=controller.boostmax
            if controller.velocidad<0:
                controller.velocidad=-controller.boostmax
         else:
             controller.avance=controller.avancebackup
             controller.velocidadmax=controller.velocidadmaxbackup

    elif key == glfw.KEY_1 and action == glfw.PRESS:
        controller.lights = not controller.lights
    elif key == glfw.KEY_3 and action == glfw.PRESS:
        controller.aesthetic = not controller.aesthetic
    else:
        return


def createVehicle():
    global  controller
    rojoOscuroR = 158 / 255
    rojoOscuroG = 45 / 255
    rojoOscuroB = 41 / 255

    rojoClaroR = 179 / 255
    rojoClaroG = 128 / 255
    rojoClaroB = 127 / 255

    baseR = 0
    baseG = 0
    baseB = 0
    quad = es.toGPUShape(bs.createColorQuad(baseR, baseG, baseB, ))
    quadRojo = es.toGPUShape(bs.createColorQuad(rojoOscuroR, rojoOscuroG, rojoOscuroB))
    quad2 = es.toGPUShape(bs.createColorQuad(1, 1, 1))
    # Chasis
    # Escala Pieza 1
    pieza1 = sg.SceneGraphNode("pieza1")
    pieza1.transform = tr.scale(1, 0.2, 1)
    pieza1.childs += [quad]
    # Traslacion pieza 1
    pieza1T = sg.SceneGraphNode("pieza1T")
    pieza1T.transform = tr.translate(-0.1, -0.2, 0)
    pieza1T.childs += [pieza1]
    # Pieza 2
    pieza2 = sg.SceneGraphNode("pieza2")
    pieza2.transform = tr.scale(0.2, 0.15, 1)
    pieza2.childs += [quad]
    # rotacion pieza 2
    pieza2R = sg.SceneGraphNode("pieza2R")
    pieza2R.transform = tr.rotationZ(-7.9)
    pieza2R.childs += [pieza2]
    # Traslacion pieza 2
    pieza2T = sg.SceneGraphNode("pieza2T")
    pieza2T.transform = tr.translate(-0.56, -0.2, 0)
    pieza2T.childs += [pieza2R]
    # Pieza 3
    # Defining locations and colors for each vertex of the shape
    vertices = [
        #   positions        colors
        0.33, -0.28, 0.0, baseR, baseG, baseB,
        0.5, -0.28, 0.0, baseR, baseG, baseB,
        0.5, -0.45, 0.0, baseR, baseG, baseB,
        0.33, -0.37, 0.0, baseR, baseG, baseB
    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
        0, 1, 2,
        2, 3, 0]

    triang = es.toGPUShape(bs.Shape(vertices, indices))
    pieza3 = sg.SceneGraphNode("pieza3")
    pieza3.transform = tr.translate(-1.13, 0.15, 0)
    pieza3.childs += [triang]

    # Pieza 4
    # Defining locations and colors for each vertex of the shape
    vertices2 = [
        #   positions        colors

        0.4, -0.25, 0.0, baseR, baseG, baseB,
        0.59, -0.25, 0.0, baseR, baseG, baseB,
        0.59, -0.27, 0.0, baseR, baseG, baseB,
        0.42, -0.37, 0.0, baseR, baseG, baseB

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices2 = [
        0, 1, 2,
        2, 3, 0]

    triang2 = es.toGPUShape(bs.Shape(vertices2, indices2))

    pieza4 = sg.SceneGraphNode("pieza 4")
    pieza4.transform = tr.scale(1.3, 1.1, 1.1)
    pieza4.childs += [triang2]
    # Traslacion pieza 4
    pieza4T = sg.SceneGraphNode("pieza4T")
    pieza4T.transform = tr.translate(-0.15, 0.1, 0)
    pieza4T.childs += [pieza4]

    # Pieza 5
    # Escala Pieza 5
    pieza5 = sg.SceneGraphNode("pieza5")
    pieza5.transform = tr.scale(1.33, 0.15, 1)
    pieza5.childs += [quadRojo]
    # Trazlacion pieza 5
    pieza5T = sg.SceneGraphNode("pieza5T")
    pieza5T.transform = tr.translate(-0.07, -0.1, 0)
    pieza5T.childs += [pieza5]

    # Pieza 6
    # Escala Pieza 6
    pieza6 = sg.SceneGraphNode("pieza6")
    pieza6.transform = tr.scale(1.12, 0.15, 1)
    pieza6.childs += [quadRojo]
    # Trazlacion pieza 6
    pieza6T = sg.SceneGraphNode("pieza6T")
    pieza6T.transform = tr.translate(-0.135, -0.05, 0)
    pieza6T.childs += [pieza6]

    # Pieza 7
    # Defining locations and colors for each vertex of the shape
    vertices3 = [
        #   positions        colors
        0.41, -0.23, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.56, -0.038, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.45, -0.09, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.55, -0.23, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices3 = [
        0, 1, 2,
        2, 3, 0]

    triang3 = es.toGPUShape(bs.Shape(vertices3, indices3))

    pieza7 = sg.SceneGraphNode("pieza7")
    pieza7.transform = tr.scale(1.3, 1.1, 1.1)
    pieza7.childs += [triang3]
    # Traslacion pieza 7
    pieza7T = sg.SceneGraphNode("pieza7T")
    pieza7T.transform = tr.translate(-1.33, 0.12, 0)
    pieza7T.childs += [pieza7]

    # Pieza 8
    # Defining locations and colors for each vertex of the shape
    vertices4 = [
        #   positions        colors
        0.4, 0.5, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.4, 0.65, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.2, 0.7, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.3, 0.5, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB
    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices4 = [
        0, 1, 2,
        2, 3, 0]

    triang4 = es.toGPUShape(bs.Shape(vertices4, indices4))

    pieza8 = sg.SceneGraphNode("pieza8")
    pieza8.transform = tr.scale(1, 1, 1)
    pieza8.childs += [triang4]
    # Traslacion pieza 8
    pieza8T = sg.SceneGraphNode("pieza8T")
    pieza8T.transform = tr.translate(0.2, -0.68, 0)
    pieza8T.childs += [pieza8]
    # Pieza 9
    # Defining locations and colors for each vertex of the shape
    vertices5 = [
        #   positions        colors
        0.55, -0.05, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.86, -0.1, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.8, -0.15, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB,
        0.5, -0.1, 0.0, rojoOscuroR, rojoOscuroG, rojoOscuroB

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices5 = [
        0, 1, 2,
        2, 3, 0]
    triang5 = es.toGPUShape(bs.Shape(vertices5, indices5))

    pieza9 = sg.SceneGraphNode("pieza9")
    pieza9.transform = tr.scale(1.3, 1.3, 1.1)
    pieza9.childs += [triang5]
    # Traslacion pieza 8
    pieza9T = sg.SceneGraphNode("pieza9T")
    pieza9T.transform = tr.translate(-1.33, 0.13, 0)
    pieza9T.childs += [pieza9]

    # Pieza 10
    # Defining locations and colors for each vertex of the shape
    vertices5 = [
        #   positions        colors
        0.4, 0.45, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        0.3, 0.51, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        0.2, 0.5, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        0.19, 0.55, 0.0, rojoClaroR, rojoClaroG, rojoClaroB

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices5 = [
        1, 0, 2,
        2, 3, 1]

    triang5 = es.toGPUShape(bs.Shape(vertices5, indices5))

    pieza10 = sg.SceneGraphNode("pieza10")
    pieza10.transform = tr.scale(1, 1, 1)
    pieza10.childs += [triang5]
    # Traslacion pieza 10
    pieza10T = sg.SceneGraphNode("pieza10T")
    pieza10T.transform = tr.translate(0.2, -0.48, 0)
    pieza10T.childs += [pieza10]

    # Pieza 11
    # Defining locations and colors for each vertex of the shape
    vertices6 = [
        #   positions        colors
        0.19, 0.55, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        0.03, 0.57, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        0.2, 0.5, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,
        -0.1, 0.505, 0.0, rojoClaroR, rojoClaroG, rojoClaroB,

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices6 = [
        1, 0, 2,
        2, 3, 1]

    triang6 = es.toGPUShape(bs.Shape(vertices6, indices6))

    pieza11 = sg.SceneGraphNode("pieza11")
    pieza11.transform = tr.scale(1, 1, 1)
    pieza11.childs += [triang6]
    # Traslacion pieza 11
    pieza11T = sg.SceneGraphNode("pieza11T")
    pieza11T.transform = tr.translate(0.2, -0.48, 0)
    pieza11T.childs += [pieza11]

    # Pieza 12
    # Defining locations and colors for each vertex of the shape
    vertices7 = [
        #   positions        colors
        -0.138, 0.759, 0.0, 0.5, 0.5, 0.5,
        0.03, 0.57, 0.0, 0.5, 0.5, 0.5,
        -0.23, 0.745, 0.0, 0.5, 0.5, 0.5,
        -0.1, 0.505, 0.0, 0.5, 0.5, 0.5

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices7 = [
        1, 0, 2,
        2, 3, 1]

    triang7 = es.toGPUShape(bs.Shape(vertices7, indices7))

    pieza12 = sg.SceneGraphNode("pieza12")
    pieza12.transform = tr.scale(1, 1, 1)
    pieza12.childs += [triang7]
    # Traslacion pieza 12
    pieza12T = sg.SceneGraphNode("pieza12T")
    pieza12T.transform = tr.translate(0.2, -0.48, 0)
    pieza12T.childs += [pieza12]

    chassis = sg.SceneGraphNode("chassis")
    chassis.transform = tr.scale(0.95, 0.6, 1)
    chassis.childs += [pieza1T]
    chassis.childs += [pieza2T]
    chassis.childs += [pieza3]
    chassis.childs += [pieza4T]
    chassis.childs += [pieza5T]
    chassis.childs += [pieza6T]
    chassis.childs += [pieza7T]
    chassis.childs += [pieza8T]
    chassis.childs += [pieza9T]
    chassis.childs += [pieza10T]
    chassis.childs += [pieza11T]
    chassis.childs += [pieza12T]
    # Wheel

    w = es.toGPUShape(bs.createCircle(22))
    wheel = sg.SceneGraphNode("wheel")
    wheel.transform = tr.uniformScale(0.18)
    wheel.childs += [w]

    wheelRotation = sg.SceneGraphNode("wheelRotation")

    wheelRotation.childs += [wheel]

    # Instanciating 2 wheels, for the front and back parts
    frontWheel = sg.SceneGraphNode("frontWheel")
    frontWheel.transform = tr.translate(0.3, -0.14, 0)
    frontWheel.childs += [wheelRotation]

    backWheel = sg.SceneGraphNode("backWheel")
    backWheel.transform = tr.translate(-0.45, -0.14, 0)
    backWheel.childs += [wheelRotation]
    #Light
    l=es.toGPUShape(bs.createColorQuad(1,1,1))
    light=sg.SceneGraphNode("Light")
    light.transform=tr.matmul([tr.translate(0.53,-0.07,0),tr.scale(0.21,0.15,1),tr.uniformScale(0.37)])
    light.childs+=[l]

    # Booster
    b = es.toGPUShape(bs.createColorQuad(baseR, baseG, baseB))
    booster = sg.SceneGraphNode("Booster")
    booster.transform = tr.matmul([tr.translate(-0.8, 0, 0), tr.scale(0.21, 0.15, 1), tr.uniformScale(1.5)])
    booster.childs += [b]

    car = sg.SceneGraphNode("Car")
    car.transform = tr.uniformScale(0.2)
    if(controller.lights==True and controller.boost==False):
        car.childs += [chassis]
        car.childs += [frontWheel]
        car.childs += [backWheel]
        car.childs += [light]
    elif (controller.lights==False and controller.boost==False):
        car.childs += [chassis]
        car.childs += [frontWheel]
        car.childs += [backWheel]
    elif (controller.lights==True and controller.boost==True):
        car.childs += [chassis]
        car.childs += [frontWheel]
        car.childs += [backWheel]
        car.childs += [light]
        car.childs +=[booster]
    else:
        car.childs += [chassis]
        car.childs += [frontWheel]
        car.childs += [backWheel]
        car.childs += [booster]

    carMove = sg.SceneGraphNode("CarM")
    carMove.childs += [car]

    return carMove


def createLandscape():
    global controller
    quad = es.toGPUShape(bs.createColorQuad(0.3, 0.3, 0.3))

    line = es.toGPUShape(bs.createColorQuad(1, 1, 1))

    streetline = sg.SceneGraphNode("streetline")
    streetline.transform = tr.scale(0.05, 0.005, 1)
    streetline.childs += [line]

    streetlinem2 = sg.SceneGraphNode("streetlinem2")
    streetlinem2.transform = tr.translate(-1.6, 0, 0)
    streetlinem2.childs += [streetline]

    streetlinem1 = sg.SceneGraphNode("streetlinem1")
    streetlinem1.transform = tr.translate(-1.4, 0, 0)
    streetlinem1.childs += [streetline]

    streetline0 = sg.SceneGraphNode("streetline0")
    streetline0.transform = tr.translate(-1.2, 0, 0)
    streetline0.childs += [streetline]

    streetline1 = sg.SceneGraphNode("streetline1")
    streetline1.transform = tr.translate(-1, 0, 0)
    streetline1.childs += [streetline]

    streetline2 = sg.SceneGraphNode("streetline2")
    streetline2.transform = tr.translate(-0.8, 0, 0)
    streetline2.childs += [streetline]

    streetline3 = sg.SceneGraphNode("streetline3")
    streetline3.transform = tr.translate(-0.6, 0, 0)
    streetline3.childs += [streetline]

    streetline4 = sg.SceneGraphNode("streetline4")
    streetline4.transform = tr.translate(-0.4, 0, 0)
    streetline4.childs += [streetline]

    streetline5 = sg.SceneGraphNode("streetline5")
    streetline5.transform = tr.translate(-0.2, 0, 0)
    streetline5.childs += [streetline]

    streetline6 = sg.SceneGraphNode("streetline6")
    streetline6.transform = tr.translate(0, 0, 0)
    streetline6.childs += [streetline]

    streetline7 = sg.SceneGraphNode("streetline7")
    streetline7.transform = tr.translate(0.2, 0, 0)
    streetline7.childs += [streetline]

    streetline8 = sg.SceneGraphNode("streetline8")
    streetline8.transform = tr.translate(0.4, 0, 0)
    streetline8.childs += [streetline]

    streetline9 = sg.SceneGraphNode("streetline9")
    streetline9.transform = tr.translate(0.6, 0, 0)
    streetline9.childs += [streetline]

    streetline10 = sg.SceneGraphNode("streetline10")
    streetline10.transform = tr.translate(0.8, 0, 0)
    streetline10.childs += [streetline]

    streetline11 = sg.SceneGraphNode("streetline11")
    streetline11.transform = tr.translate(1, 0, 0)
    streetline11.childs += [streetline]

    streetline12 = sg.SceneGraphNode("streetline12")
    streetline12.transform = tr.translate(1.2, 0, 0)
    streetline12.childs += [streetline]

    streetline13 = sg.SceneGraphNode("streetline13")
    streetline13.transform = tr.translate(1.4, 0, 0)
    streetline13.childs += [streetline]

    streetline14 = sg.SceneGraphNode("streetline14")
    streetline14.transform = tr.translate(1.6, 0, 0)
    streetline14.childs += [streetline]

    streetline15 = sg.SceneGraphNode("streetline15")
    streetline15.transform = tr.translate(1.8, 0, 0)
    streetline15.childs += [streetline]

    street = sg.SceneGraphNode("streetNode")
    street.transform = tr.scale(2, 0.1, 1)
    street.childs += [quad]

    streetpers = sg.SceneGraphNode("Street")
    streetpers.transform = tr.translate(0, -0.05, 0)
    streetpers.childs += [street]

    streetpers.childs += [streetline1]
    streetpers.childs += [streetline2]
    streetpers.childs += [streetline3]
    streetpers.childs += [streetline4]
    streetpers.childs += [streetline5]
    streetpers.childs += [streetline6]
    streetpers.childs += [streetline7]
    streetpers.childs += [streetline8]
    streetpers.childs += [streetline9]
    streetpers.childs += [streetline10]
    streetpers.childs += [streetline11]

    streetI = sg.SceneGraphNode("StreetI")
    streetI.transform = tr.translate(controller.si, 0, 0)
    streetI.childs += [streetpers]

    streetD = sg.SceneGraphNode("StreetD")
    streetD.transform = tr.translate(controller.sc, 0, 0)
    streetD.childs += [streetpers]

    streetC = sg.SceneGraphNode("StreetC")
    streetC.transform = tr.translate(controller.sd, 0, 0)
    streetC.childs += [streetpers]

    # Palmera

    # Tronco
    woodR = 183 / 255
    woodG = 126 / 255
    woodB = 27 / 255
    tronco = es.toGPUShape(bs.createColorQuad(woodR, woodG, woodB))
    tronconode = sg.SceneGraphNode("Tronco")
    tronconode.transform = tr.matmul([tr.scale(0.05, 0.2, 1), tr.translate(-10, 0.02, 0)])
    tronconode.childs += [tronco]
    #Hoja
    hojaR = 41/ 255
    hojaG = 145 / 255
    hojaB = 13 / 255

    vertices = [
        #   positions        colors
        1.3, -4.5, 0.0, hojaR, hojaG, hojaB,
        -0.5, -0.8, 0.0, hojaR, hojaG, hojaB,
        -0.5, 1.5, 0.0, hojaR, hojaG, hojaB,
        1, -1, 0.0, hojaR, hojaG, hojaB,
    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
        0, 1, 2,
        2, 3, 0,]

    hoja = es.toGPUShape(bs.Shape(vertices, indices))
    hojanode = sg.SceneGraphNode("Hoja")
    hojanode.transform = tr.matmul([tr.scale(0.1, 0.05, 1),tr.uniformScale(0.3), tr.translate(-16.1, 7, 0)])
    hojanode.childs += [hoja]

    hoja2=sg.SceneGraphNode("Hoja2")
    hoja2.transform=tr.matmul([tr.translate(-1.10,0,0),tr.scale(2,1,0),tr.rotationY(180)])
    hoja2.childs+=[hojanode]


    palmnode=sg.SceneGraphNode("PalmnNode")

    palmnode.childs += [tronconode]
    palmnode.childs += [hojanode]
    palmnode.childs += [hoja2]

    # Palmera 1
    palm1 = sg.SceneGraphNode("Palm1")
    palm1.transform = tr.matmul([tr.uniformScale(1.15), tr.translate(0, 0.03, 0)])
    palm1.childs += [palmnode]

    # Palmera 2
    palm2 = sg.SceneGraphNode("Palm2")
    palm2.transform = tr.matmul([tr.scale(0.8,0.5,1), tr.translate(0.8, 0.05, 0)])
    palm2.childs += [palmnode]

    # Palmera 3
    palm3 = sg.SceneGraphNode("Palm3")
    palm3.transform = tr.matmul([tr.scale(3, 0.5, 1), tr.translate(0.7, 0.15, 0)])
    palm3.childs += [palmnode]

    # Sand
    sandR = 242 / 255
    sandG = 209/ 255
    sandB = 107 / 255
    sand = es.toGPUShape(bs.createColorQuad(sandR, sandG, sandB))
    sandnode = sg.SceneGraphNode("Sand")
    sandnode.transform = tr.matmul([tr.scale(2, 0.05, 1)])
    sandnode.childs += [sand]

    sandBack1 = sg.SceneGraphNode("SandBack1")
    sandBack1.transform = tr.matmul([tr.translate(0, 0.09, 0), tr.scale(1, 1.3, 1)])
    sandBack1.childs += [sandnode]
    sandBack1.childs += [palm2]

    sandBackI1 = sg.SceneGraphNode("SandBackI1")
    sandBackI1.transform = tr.translate(controller.sandi, 0, 0)
    sandBackI1.childs += [sandBack1]

    sandBackD1 = sg.SceneGraphNode("SandBackD1")
    sandBackD1.transform = tr.translate(controller.sandd, 0, 0)
    sandBackD1.childs += [sandBack1]

    sandBackC1 = sg.SceneGraphNode("SandBackC1")
    sandBackC1.transform = tr.translate(controller.sandc, 0, 0)
    sandBackC1.childs += [sandBack1]

    #SandBack 2
    sandBack2 = sg.SceneGraphNode("SandBack2")
    sandBack2.transform = tr.matmul([tr.translate(0, 0.03, 0), tr.scale(1, 1.3, 1)])
    sandBack2.childs += [sandnode]
    sandBack2.childs += [palm1]

    sandBackI2 = sg.SceneGraphNode("SandBackI2")
    sandBackI2.transform = tr.translate(controller.sandi2, 0, 0)
    sandBackI2.childs += [sandBack2]

    sandBackD2 = sg.SceneGraphNode("SandBackD2")
    sandBackD2.transform = tr.translate(controller.sandd2, 0, 0)
    sandBackD2.childs += [sandBack2]

    sandBackC2 = sg.SceneGraphNode("SandBackC2")
    sandBackC2.transform = tr.translate(controller.sandc2, 0, 0)
    sandBackC2.childs += [sandBack2]

    #SandFront 1
    sandFront1 = sg.SceneGraphNode("SandFront1")
    sandFront1.transform = tr.matmul([tr.translate(0, -0.226, 0), tr.scale(1, 5, 1)])
    sandFront1.childs += [sandnode]


    sandFrontI1 = sg.SceneGraphNode("SandFrontI1")
    sandFrontI1.transform = tr.translate(controller.sandfi, 0, 0)
    sandFrontI1.childs += [sandFront1]

    sandFrontD1 = sg.SceneGraphNode("SandFrontD1")
    sandFrontD1.transform = tr.translate(controller.sandfd, 0, 0)
    sandFrontD1.childs += [sandFront1]

    sandFrontC1 = sg.SceneGraphNode("SandFrontC1")
    sandFrontC1.transform = tr.translate(controller.sandfc, 0, 0)
    sandFrontC1.childs += [sandFront1]

    # SandFront 2
    sandFront2 = sg.SceneGraphNode("SandFront2")
    sandFront2.transform = tr.matmul([tr.translate(0, -0.55, 0), tr.scale(1, 8, 1)])
    sandFront2.childs += [sandnode]

    sandFrontI2 = sg.SceneGraphNode("SandFrontI2")
    sandFrontI2.transform = tr.translate(controller.sandfi2, 0, 0)
    sandFrontI2.childs += [sandFront2]

    sandFrontD2 = sg.SceneGraphNode("SandFrontD2")
    sandFrontD2.transform = tr.translate(controller.sandfd2, 0, 0)
    sandFrontD2.childs += [sandFront2]

    sandFrontC2 = sg.SceneGraphNode("SandFront2")
    sandFrontC2.transform = tr.translate(controller.sandfc2, 0, 0)
    sandFrontC2.childs += [sandFront2]

    # SandFront 3
    sandFront3 = sg.SceneGraphNode("SandFront3")
    sandFront3.transform = tr.matmul([tr.translate(0, -0.88, 0), tr.scale(1, 5.2, 1)])
    sandFront3.childs += [sandnode]
    sandFront3.childs+=[palm3]

    sandFrontI3 = sg.SceneGraphNode("SandFrontI3")
    sandFrontI3.transform = tr.translate(controller.sandfi3, 0, 0)
    sandFrontI3.childs += [sandFront3]

    sandFrontD3 = sg.SceneGraphNode("SandFrontD3")
    sandFrontD3.transform = tr.translate(controller.sandfd3, 0, 0)
    sandFrontD3.childs += [sandFront3]

    sandFrontC3 = sg.SceneGraphNode("SandFront2")
    sandFrontC3.transform = tr.translate(controller.sandfc3, 0, 0)
    sandFrontC3.childs += [sandFront3]
    #Boat
    boatc=es.toGPUShape(bs.createColorQuad(0,0,0))
    verticesd = [
        #   positions        colors
        0.5, -0.5, 0.0, 0, 0, 0,
        0.5, 0.5, 0.0, 0, 0, 0,
        -0.5, -0.5, 0.0, 0,0, 0


    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indicesd = [
        1, 2, 0
       ]
    boatd = es.toGPUShape(bs.Shape(verticesd, indicesd))
    verticesi = [
        #   positions        colors
        -0.5, 0.5, 0.0, 0, 0, 0,
        -0.5, -0.5, 0.0, 0, 0, 0,
        0.5, -0.5, 0.0, 0, 0, 0

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indicesi = [
        0, 1, 2
    ]


    boati = es.toGPUShape(bs.Shape(verticesi, indicesi))
    boatcnode=sg.SceneGraphNode("Boatc")
    boatcnode.transform=tr.matmul([tr.translate(0,0,0),tr.scale(1,0.6,1)])
    boatcnode.childs+=[boatc]

    boatdnode = sg.SceneGraphNode("Boatd")
    boatdnode.transform = tr.translate(0.15,0.2,0)
    boatdnode.childs += [boatd]

    boatinode = sg.SceneGraphNode("Boati")
    boatinode.transform = tr.translate(-0.15, 0.2, 0)
    boatinode.childs += [boati]
    verticesb = [
        #   positions        colors
        0.5, 0.5, 0.0, 0, 0, 0,
        0.5, -0.5, 0.0, 0, 0, 0,
        -0.5, -0.5, 0.0, 0, 0, 0

    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indicesb = [
        0, 1, 2
    ]
    boatb = es.toGPUShape(bs.Shape(verticesb, indicesb))
    boatbnode=sg.SceneGraphNode("Boatb")
    boatbnode.transform=tr.matmul([tr.translate(0,0.35,0),tr.uniformScale(0.05)])
    boatbnode.childs+=[boatb]


    boat=sg.SceneGraphNode("Boat")
    boat.transform=tr.matmul([tr.translate(0,0.3,0),tr.uniformScale(0.05),tr.scale(1.2,1,1)])
    boat.childs += [boatcnode]
    boat.childs += [boatdnode]
    boat.childs += [boatinode]

    boatf=sg.SceneGraphNode("Boatf")
    boatf.transform=tr.translate(controller.boat,-0.1,0)
    boatf.childs+=[boat]
    boatf.childs+=[boatbnode]



    # Sea
    seaR = 17 / 255
    seaG = 118 / 255
    seaB = 222 / 255
    sea = es.toGPUShape(bs.createColorQuad(seaR, seaG, seaB))
    seanode = sg.SceneGraphNode("SeaNode")
    seanode.transform = tr.matmul([tr.scale(2, 0.1, 1),tr.translate(0,1.7,0)])
    seanode.childs += [sea]

    sea = sg.SceneGraphNode("Sea")
    sea.childs+=[seanode]
    sea.childs+=[boatf]

    seaC=sg.SceneGraphNode("SeaC")
    seaC.transform=tr.translate(controller.seac,0,0)
    seaC.childs+=[sea]

    seaD = sg.SceneGraphNode("SeaD")
    seaD.transform = tr.translate(controller.sead, 0, 0)
    seaD.childs += [sea]

    seaI = sg.SceneGraphNode("SeaI")
    seaI.transform = tr.translate(controller.seai, 0, 0)
    seaI.childs += [sea]


    nubecore=es.toGPUShape(bs.createCirclergb(10,1,1,1))
    nubenode=sg.SceneGraphNode("Nubenode")
    nubenode.transform=tr.matmul([tr.translate(0,0.5,0),tr.uniformScale(0.1)])
    nubenode.childs+=[nubecore]

    nube1=sg.SceneGraphNode("Nube1")
    nube1.transform=tr.matmul([tr.translate(-0.05,0.05,0)])
    nube1.childs+=[nubenode]

    nube2 = sg.SceneGraphNode("Nube2")
    nube2.transform = tr.matmul([tr.translate(0.05, 0.05, 0)])
    nube2.childs += [nubenode]

    nube3 = sg.SceneGraphNode("Nube3")
    nube3.transform = tr.matmul([tr.translate(-0.06, -0.01, 0)])
    nube3.childs += [nubenode]

    nube4 = sg.SceneGraphNode("Nube4")
    nube4.transform = tr.matmul([tr.translate(0.02, -0.03, 0)])
    nube4.childs += [nubenode]

    nube5 = sg.SceneGraphNode("Nube5")
    nube5.transform = tr.matmul([tr.translate(0.05, -0.01, 0)])
    nube5.childs += [nubenode]

    nubef=sg.SceneGraphNode("Nubef")
    nubef.transform = tr.matmul([tr.scale(1.2,0.8,1)])
    nubef.childs+=[nubenode]
    nubef.childs+=[nube1]
    nubef.childs += [nube2]
    nubef.childs += [nube3]
    nubef.childs += [nube4]
    nubef.childs += [nube5]

    nubef1 = sg.SceneGraphNode("Nubef1")
    nubef1.transform =tr.translate(controller.nube, 0.5, 0)
    nubef1.childs += [nubef]

    nubef2=sg.SceneGraphNode("Nubef2")
    nubef2.transform = tr.matmul([tr.translate(controller.nube2, 0.4, 0), tr.scale(0.8, 0.7, 1)])
    nubef2.childs += [nubef]

    nubef3 = sg.SceneGraphNode("Nubef3")
    nubef3.transform = tr.matmul([tr.translate(controller.nube3, 0.3, 0), tr.scale(0.5, 1, 1)])
    nubef3.childs += [nubef]
    # Sky
    if controller.aesthetic==False:
        if (controller.sun < 0.3):
            skyR = 237 / 255
            skyG = 59 / 255
            skyB = 49 / 255
        elif (controller.sun >= 0.3 and controller.sun < 0.5):
            skyR = 254 / 255
            skyG = 145 / 255
            skyB = 0 / 255
        else:
            skyR = 104 / 255
            skyG = 191 / 255
            skyB = 245 / 255
    else:
        skyR = 104 / 255
        skyG = 191 / 255
        skyB = 245 / 255




    sky = es.toGPUShape(bs.createColorQuad(skyR, skyG, skyB))
    skynode = sg.SceneGraphNode("SkyNode")
    skynode.transform = tr.matmul([tr.scale(2, 0.8, 1), tr.translate(0, 0.75, 0)])
    skynode.childs += [sky]

    sky= sg.SceneGraphNode("Sky")
    sky.childs += [skynode]

    sunf=es.toGPUShape(bs.createCirclergb(30,1,1,0))
    sunnode=sg.SceneGraphNode("Sun")
    sunnode.transform= tr.matmul([tr.uniformScale(0.5),tr.translate(0,0.2+controller.sun,0)])
    sunnode.childs+=[sunf]
    if(controller.aesthetic==False):
        sky.childs += [sunnode]
        sky.childs += [nubef1]
        sky.childs += [nubef2]
        sky.childs += [nubef3]
    else:
        sky.childs += [sunnode]


    skyC = sg.SceneGraphNode("SkyC")
    skyC.transform = tr.translate(controller.skyc, 0, 0)
    skyC.childs += [sky]

    skyD = sg.SceneGraphNode("SkyD")
    skyD.transform = tr.translate(controller.skyd, 0, 0)
    skyD.childs += [sky]

    skyI = sg.SceneGraphNode("SkyI")
    skyI.transform = tr.translate(controller.skyi, 0, 0)
    skyI.childs += [sky]


    landscape = sg.SceneGraphNode("Landscape")

    landscape.childs += [skyI]
    landscape.childs += [skyD]
    landscape.childs += [skyC]

    landscape.childs += [seaI]
    landscape.childs += [seaD]
    landscape.childs += [seaC]

    landscape.childs += [sandBackI1]
    landscape.childs += [sandBackC1]
    landscape.childs += [sandBackD1]

    landscape.childs += [sandBackI2]
    landscape.childs += [sandBackC2]
    landscape.childs += [sandBackD2]

    landscape.childs += [streetI]
    landscape.childs += [streetC]
    landscape.childs += [streetD]

    landscape.childs += [sandFrontI1]
    landscape.childs += [sandFrontC1]
    landscape.childs += [sandFrontD1]

    landscape.childs += [sandFrontI2]
    landscape.childs += [sandFrontC2]
    landscape.childs += [sandFrontD2]

    landscape.childs += [sandFrontI3]
    landscape.childs += [sandFrontC3]
    landscape.childs += [sandFrontD3]



    return landscape


# controller.reset()
def loop():
    global controller

    if (controller.derecha == True):
        # Loop street
        if (controller.si < -2):
            controller.si = controller.sd + 2
        elif (controller.sc < -2):
            controller.sc = controller.si + 2
        elif (controller.sd < -2):
            controller.sd = controller.sc + 2
        # SandBack1
        if (controller.sandi < -2):
            controller.sandi = controller.sandd + 2
        elif (controller.sandc < -2):
            controller.sandc = controller.sandi + 2
        elif (controller.sandd < -2):
            controller.sandd = controller.sandc + 2
        # SandBack2
        if (controller.sandi2 < -2):
            controller.sandi2 = controller.sandd2 + 2
        elif (controller.sandc2 < -2):
            controller.sandc2 = controller.sandi2 + 2
        elif (controller.sandd2 < -2):
            controller.sandd2 = controller.sandc2 + 2
        # Sea
        if (controller.seai < -2):
            controller.seai = controller.sead + 2
        elif (controller.seac < -2):
             controller.seac = controller.seai + 2
        elif (controller.sead < -2):
             controller.sead = controller.seac + 2
        if (controller.skyi < -2):
            controller.skyi = controller.skyd + 2
        elif (controller.skyc < -2):
             controller.skyc = controller.skyi + 2
        elif (controller.skyd < -2):
             controller.skyd = controller.skyc + 2
         # SandFront1
        if (controller.sandfi < -2):
            controller.sandfi = controller.sandfd + 2
        elif (controller.sandfc < -2):
            controller.sandfc = controller.sandfi + 2
        elif (controller.sandfd < -2):
            controller.sandfd = controller.sandfc + 2
        # SandFront2
        if (controller.sandfi2 < -2):
            controller.sandfi2 = controller.sandfd2 + 2
        elif (controller.sandfc2 < -2):
            controller.sandfc2 = controller.sandfi2 + 2
        elif (controller.sandfd2 < -2):
             controller.sandfd2 = controller.sandfc2 + 2
        # SandFront3
        if (controller.sandfi3 < -2):
            controller.sandfi3 = controller.sandfd3 + 2
        elif (controller.sandfc3 < -2):
            controller.sandfc3 = controller.sandfi3 + 2
        elif (controller.sandfd3 < -2):
            controller.sandfd3 = controller.sandfc3 + 2
    if (controller.izquierda == True):
        # Street
        if (controller.sd > 2):
            controller.sd = controller.si - 2
        elif (controller.sc > 2):
            controller.sc = controller.sd - 2
        elif (controller.si > 2):
            controller.si = controller.sc - 2
        # SandBack1
        if (controller.sandd > 2):
            controller.sandd = controller.sandi - 2
        elif (controller.sandc > 2):
            controller.sandc = controller.sandd - 2
        elif (controller.sandi > 2):
            controller.sandi = controller.sandc - 2
        # SandBack2
        if (controller.sandd2 > 2):
            controller.sandd2 = controller.sandi2 - 2
        elif (controller.sandc2 > 2):
            controller.sandc2 = controller.sandd2 - 2
        elif (controller.sandi2 > 2):
            controller.sandi2 = controller.sandc2 - 2
        # Sea
        if (controller.sead > 2):
            controller.sead = controller.seai - 2
        elif (controller.seac > 2):
            controller.seac = controller.sead - 2
        elif (controller.seai > 2):
            controller.seai = controller.seac - 2
        if (controller.skyd > 2):
            controller.skyd = controller.seai - 2
        elif (controller.skyc > 2):
            controller.skyc = controller.sead - 2
        elif (controller.skyi > 2):
            controller.skyi = controller.skyc - 2
        # SandFront1
        if (controller.sandfd > 2):
            controller.sandfd = controller.sandfi - 2
        elif (controller.sandfc > 2):
            controller.sandfc = controller.sandfd - 2
        elif (controller.sandfi > 2):
            controller.sandfi = controller.sandfc - 2
        # SandFront2
        if (controller.sandfd2 > 2):
             controller.sandfd2 = controller.sandfi2 - 2
        elif (controller.sandfc2 > 2):
            controller.sandfc2 = controller.sandfd2 - 2
        elif (controller.sandfi2 > 2):
             controller.sandfi2 = controller.sandfc2 - 2
        # SandFront3
        if (controller.sandfd3 > 2):
               controller.sandfd3 = controller.sandfi3 - 2
        elif (controller.sandfc3 > 2):
             controller.sandfc3 = controller.sandfd3 - 2
        elif (controller.sandfi3 > 2):
            controller.sandfi3 = controller.sandfc3 - 2
    #Bote
    if(controller.boat> 1):
        controller.boat= -1
    # Nube
    if (controller.nube > 1.2):
        controller.nube= -1.2
    if (controller.nube2 > 1.2):
        controller.nube2 = -1.2
    if (controller.nube3 < -1.2):
        controller.nube3 = 1.2
    return


def stop():
    global controller
    if controller.moviendose == False and controller.velocidad != 0:

        if controller.velocidad < 0 and controller.velocidad <= -0.002:
            controller.velocidad += 0.0001
        elif controller.velocidad > 0 and controller.velocidad >= 0.002:
            controller.velocidad += -0.0001
        else:
            controller.velocidad = 0

    return


def move():
    global controller
    # SandBack1
    controller.sandc += controller.velocidad / 3
    controller.sandd += controller.velocidad / 3
    controller.sandi += controller.velocidad / 3
    # SandBack2
    controller.sandc2 += controller.velocidad / 2
    controller.sandd2 += controller.velocidad / 2
    controller.sandi2 += controller.velocidad / 2
    # Street
    controller.sc += controller.velocidad
    controller.sd += controller.velocidad
    controller.si += controller.velocidad
    #Sea
    controller.seac += controller.velocidad / 7
    controller.sead += controller.velocidad / 7
    controller.seai += controller.velocidad / 7

    # Sky
    controller.skyc += 0
    controller.skyd += 0
    controller.skyi += 0

    # SandFront1
    controller.sandfc += controller.velocidad *1.5
    controller.sandfd += controller.velocidad *1.5
    controller.sandfi += controller.velocidad *1.5
    # SandFront2
    controller.sandfc2 += controller.velocidad *1.8
    controller.sandfd2 += controller.velocidad *1.8
    controller.sandfi2 += controller.velocidad *1.8
    # SandFront3
    controller.sandfc3 += controller.velocidad *2
    controller.sandfd3 += controller.velocidad *2
    controller.sandfi3 += controller.velocidad *2

    #Wheel rotation
    wheelRotationNode = sg.findNode(car, "wheelRotation")

    controller.rotation += controller.velocidad * 6
    wheelRotationNode.transform = tr.rotationZ(controller.rotation)

    #Movimiento bote
    controller.boat+=controller.boatvel
    #Movimiento sol
    if controller.aesthetic == False:
        if (controller.sun <= 1.1):
            controller.sun += controller.sunvel
    else:
        controller.sun=0

    #Movimiento nube
    controller.nube +=controller.nubevel
    controller.nube2 += controller.nubevel/2
    controller.nube3 += -controller.nubevel *1.1

    return


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    window = glfw.create_window(width, height, "Drawing a Quad via a EBO", None, None)
    # window = glfw.create_window(width, height, "Drawing a Quad via a EBO", glfw.get_primary_monitor(), None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)
    # glfw.set_input_mode(window, glfw.STICKY_KEYS, True)

    # Assembling the shader program (pipeline) with both shaders
    pipeline = es.SimpleTransformShaderProgram()

    # Telling OpenGL to use our shader program
    glUseProgram(pipeline.shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # Creating shapes on GPU memory
    landscape = createLandscape()


    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    t0 = glfw.get_time()
    while not glfw.window_should_close(window):
        car = createVehicle()
        # Getting the time difference from the previous iteration
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1


        # Movimiento
        move()


        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glUseProgram(pipeline.shaderProgram)

        # Creating landscape
        landscape = createLandscape()
        sg.drawSceneGraphNode(landscape, pipeline, "transform")
        sg.drawSceneGraphNode(car, pipeline, "transform")

        # Movement stop
        stop()

        # Looping landscape
        loop()

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    glfw.terminate()
