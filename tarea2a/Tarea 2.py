import scene_graph as sg
import basic_shapes as bs
import easy_shaders as es

import transformations as tr
import lighting_shaders as ls
import glfw
from OpenGL.GL import *
import numpy as np
import sys
from PIL import Image

INT_BYTES = 4


# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.showAxis = False
        # Positions
        self.x = 0
        self.y = 0
        self.z = 0
        self.x_start = 0
        self.y_start = 0
        # Map requirements
        self.start = False
        self.treasureP = False

        self.camera = 0.4
        self.follow_player = True
        self.zoom = 300
        self.camera_theta = np.pi / 2
        self.camera_rho = 2 * np.pi
        self.camera_theta_sky = -0.025
        self.camera_rho_sky = 13.381853580769175
        self.fov = 90
        self.fast = False
        self.veloc = 2
        self.fullScreen = True

        # Collisions
        self.tolerance = 0.25
        self.treasureClose = 0.25
        self.collisions = np.array([])
        self.treasureColl = np.array([])

        # Maze
        self.walls = np.array([])
        self.treasure = np.array([])

        self.noclip = False

        # Lighting
        self.quadratic = 0.7
        self.lineal = -0.69
        self.constant = 0.6

        self.night = False


# We will use the global controller as communication with the callback function
controller = Controller()  # Here we declare this as a global variable.


def on_key(window, key, scancode, action, mods):
    global controller  # Declares that we are going to use the global object controller inside this function.

    if action == glfw.PRESS and key == glfw.KEY_P:
        controller.fillPolygon = not controller.fillPolygon
        # print("Toggle GL_FILL/GL_LINE")

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

    elif action == glfw.PRESS and key == glfw.KEY_O:
        controller.follow_player = not controller.follow_player
    elif action == glfw.PRESS and key == glfw.KEY_LEFT_SHIFT:
        if controller.fast:
            controller.veloc = 5
        else:
            controller.veloc = 2
        controller.fast = not controller.fast
    elif action == glfw.PRESS and mods == glfw.MOD_ALT and key == glfw.KEY_ENTER:
        if not controller.fullScreen:
            glfw.set_window_monitor(window, glfw.get_primary_monitor(), 0, 0, width, height, 60)
        else:
            glfw.set_window_monitor(window, None, 0, 0, width, height, 60)
        controller.fullScreen = not controller.fullScreen
    elif action == glfw.PRESS and key == glfw.KEY_N:
        controller.noclip = not controller.noclip
    elif action == glfw.PRESS and key == glfw.KEY_SPACE:
        controller.night = not controller.night


# Create ground with textures
def createGround():
    gpuGround_texture = es.toGPUShape(bs.createTextureQuadNormals("Textures/moon.png"), GL_REPEAT, GL_NEAREST)
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr.scale(1, 1, 1)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    return ground_rotated


# Create walls
def createWall():
    gpuWall_texture = es.toGPUShape(bs.createTextureNormalsCube("Textures/wall3.png"), GL_REPEAT, GL_NEAREST)
    wall_scaled = sg.SceneGraphNode("wall_scaled")
    wall_scaled.transform = tr.scale(1, 1, 1)
    wall_scaled.childs += [gpuWall_texture]

    wall_rotated = sg.SceneGraphNode("wall_rotated_x")
    wall_rotated.transform = tr.rotationX(0)
    wall_rotated.childs += [wall_scaled]

    return wall_rotated


def createTreasure():
    gpuTreasure_texture = es.toGPUShape(bs.createTextureNormalsCube("Textures/crate.png"), GL_REPEAT, GL_NEAREST)
    treasure_scaled = sg.SceneGraphNode("treasure_scaled")
    treasure_scaled.transform = tr.scale(1, 1, 1)
    treasure_scaled.childs += [gpuTreasure_texture]

    treasure_rotated = sg.SceneGraphNode("wall_rotated_x")
    treasure_rotated.transform = tr.rotationX(0)
    treasure_rotated.childs += [treasure_scaled]

    gpuGold_texture = es.toGPUShape(bs.createTextureQuadNormals("Textures/gold.jpg"), GL_REPEAT, GL_NEAREST)

    gold_scaled = sg.SceneGraphNode("gold_scaled")
    gold_scaled.transform = tr.uniformScale(0.85)
    gold_scaled.childs += [gpuGold_texture]

    gold_rotated = sg.SceneGraphNode("gold_rotated")
    gold_rotated.transform = tr.translate(0, 0, 0.505)
    gold_rotated.childs += [gold_scaled]

    treasure_complete = sg.SceneGraphNode("treasure_complete")
    treasure_complete.childs += [gold_rotated]
    treasure_complete.childs += [treasure_rotated]

    return treasure_complete


# Create Maze
def createMaze(mazearg):
    global controller
    grid = np.load(mazearg)
    groundr = createGround()
    treasurer = createTreasure()
    wallr = createWall()

    for x in range(grid.shape[0]):
        for y in range(grid.shape[1]):
            if grid[x, y] == 1:
                wall = sg.SceneGraphNode("wall")
                wall.transform = tr.translate(x, y, 0)
                wall.childs += [wallr]
                controller.walls = np.append(controller.walls, wall)

                if (controller.collisions.size == 0):
                    controller.collisions = np.array([x + 0.5, y + 0.5])
                    controller.collisions = np.vstack((np.array([x - 0.5, y - 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x - 0.5, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x + 0.5, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y - 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y + 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x - 0.5, y + 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x + 0.5, y - 0.5]), controller.collisions))
                else:
                    controller.collisions = np.vstack((np.array([x + 0.5, y + 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x - 0.5, y - 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x - 0.5, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x + 0.5, y]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y - 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x, y + 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x - 0.5, y + 0.5]), controller.collisions))
                    controller.collisions = np.vstack((np.array([x + 0.5, y - 0.5]), controller.collisions))

            elif grid[x, y] == 2:
                controller.x = x
                controller.y = y
                controller.x_start = x
                controller.y_start = y
                controller.start = True
                ground = sg.SceneGraphNode("ground")
                ground.transform = tr.translate(x, y, -0.5)
                ground.childs += [groundr]
                controller.walls = np.append(controller.walls, ground)
            elif grid[x, y] == 0:
                ground = sg.SceneGraphNode("ground")
                ground.transform = tr.translate(x, y, -0.5)
                ground.childs += [groundr]
                controller.walls = np.append(controller.walls, ground)
            elif grid[x, y] == 3:
                controller.treasureP = True
                ground = sg.SceneGraphNode("treasure_transform")
                ground.transform = tr.translate(x, y, -0.5)
                ground.childs += [groundr]
                controller.walls = np.append(controller.walls, ground)

                treasure = sg.SceneGraphNode("treasure")
                treasure.transform = tr.matmul([tr.translate(x, y, -0.36), tr.scale(0.5, 0.5, 0.3)])
                treasure.childs += [treasurer]
                controller.treasure = np.append(controller.treasure, treasure)

                if controller.treasureColl.size == 0:
                    controller.treasureColl = np.array([x + 0.5, y + 0.5])
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y - 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x + 0.5, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y - 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y + 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y + 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x + 0.5, y - 0.5]), controller.treasureColl))
                else:
                    controller.treasureColl = np.vstack((np.array([x + 0.5, y + 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y - 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x + 0.5, y]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y - 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x, y + 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x - 0.5, y + 0.5]), controller.treasureColl))
                    controller.treasureColl = np.vstack((np.array([x + 0.5, y - 0.5]), controller.treasureColl))

    if controller.start == False:
        raise SystemError('No start point found')
    if controller.treasureP == False:
        raise SystemError('No treasures found')
    return


def scroll_callback(window, xoffset, yoffset):
    if (yoffset > 0):
        controller.fov -= 5
    elif (yoffset < 0):
        controller.fov += 5


def checkClose(array, test, close):
    for i in array:
        if np.abs(i[0] - test[0]) < close and np.abs(i[1] - test[1]) < close:
            return True
    return False


def getCol(array, test, close):
    for i in array:
        if np.abs(i[0] - test[0]) <= close and np.abs(i[1] - test[1]) <= close:
            return i


def createLight():
    gpuWall_texture = es.toGPUShape(bs.createColorNormalsCube(1, 1, 1))
    wall_scaled = sg.SceneGraphNode("wall_scaled")
    wall_scaled.transform = tr.scale(0.2, 0.2, 0.2)
    wall_scaled.childs += [gpuWall_texture]

    wall_rotated = sg.SceneGraphNode("wall_rotated_x")
    wall_rotated.transform = tr.translate(controller.x, controller.y, 3)
    wall_rotated.childs += [wall_scaled]

    return wall_rotated


def check(test, array):
    return any(np.array_equal(x, test) for x in array)


def loadCubeMap(faces):
    skyt = glGenTextures(1)
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyt)

    for i in range(len(faces)):
        image = Image.open(faces[i])
        img_data = np.array(list(image.getdata()), np.uint8)

        if image.mode == "RGB":
            internalFormat = GL_RGB
            format = GL_RGB
        elif image.mode == "RGBA":
            internalFormat = GL_RGBA
            format = GL_RGBA
        else:
            print("Image mode not supported.")
            raise Exception()

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, image.size[0], image.size[1], 0, format,
                     GL_UNSIGNED_BYTE,
                     img_data)
        # glGenerateMipmap(GL_TEXTURE_2D)

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE)
    return skyt


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1920
    height = 1080

    # width = 800
    # height = 600

    if controller.fullScreen:
        window = glfw.create_window(width, height, "Window name", glfw.get_primary_monitor(), None)
    else:
        window = glfw.create_window(width, height, "Window name", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with both shaders

    phongPipeline = ls.SimpleTexturePhongShaderProgram()

    TextureShader = es.SimpleTextureModelViewProjectionShaderProgram()

    TextureShaderSky = es.SimpleShaderProgramSkyBox()

    # Telling OpenGL to use our shader program

    glUseProgram(TextureShader.shaderProgram)

    glEnable(GL_DEPTH_TEST)
    # Setting up the clear screen color
    glClearColor(0, 1, 1, 1.0)

    # Creating shapes on GPU memory
    vertices = [

        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,

        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,

        1.0, -1.0, -1.0,
        1.0, -1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,

        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,

        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0
    ]
    skyboxVertices = np.array(vertices, dtype=np.float32)
    skyboxVAO = glGenVertexArrays(1)
    skyboxVBO = glGenBuffers(1)
    glBindVertexArray(skyboxVAO)
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO)
    glBufferData(GL_ARRAY_BUFFER, len(skyboxVertices) * INT_BYTES, skyboxVertices, GL_STATIC_DRAW)
    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), ctypes.c_void_p(0))
    glBindVertexArray(0)

    facesDay = np.array(
        ["sky/right.jpg", "sky/left.jpg", "sky/top.jpg", "sky/bottom.jpg", "sky/front.jpg", "sky/back.jpg"])
    facesNight = np.array(
        ["sky NEBULA/1.tga", "sky NEBULA/2.tga", "sky NEBULA/3.tga", "sky NEBULA/4.tga", "sky NEBULA/5.tga",
         "sky NEBULA/6.tga"])

    cubeMapTextureDay = loadCubeMap(facesDay)
    cubeMapTextureNight = loadCubeMap(facesNight)

    # Create maze from command line argument 1 and store it in an array
    createMaze(sys.argv[1])
    np.save('maze.txt', controller.collisions)

    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    # projection = tr.ortho(-1, 1, -1, 1, 0.1, 20)
    projection = tr.perspective(controller.fov, float(width) / float(height), 0.1, 300)

    model = tr.identity()

    # Initial values, used for calculating time and pos differences
    t0 = glfw.get_time()
    xpos0 = glfw.get_cursor_pos(window)[0]
    ypos0 = glfw.get_cursor_pos(window)[1]
    ti = glfw.get_time()

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        glfw.set_input_mode(window, glfw.CURSOR, glfw.CURSOR_DISABLED)

        projection = tr.perspective(controller.fov, float(width) / float(height), 0.1, 300)

        # Filling or not the shapes depending on the controller state
        if controller.fillPolygon:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Getting the time and pos difference from the previous iteration
        # Time
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1

        # X Pos
        xpos = glfw.get_cursor_pos(window)[0]
        dposx = xpos - xpos0
        xpos0 = xpos
        # Y Pos
        ypos = glfw.get_cursor_pos(window)[1]
        dposy = ypos - ypos0
        ypos0 = ypos

        if 3.14 > controller.camera_theta + (dposy * dt / 10) > 0.1:
            controller.camera_theta += (dposy * dt / 10)
            controller.camera_theta_sky -= (dposy * dt / 10)

        controller.camera_rho -= (dposx * dt / 10)
        controller.camera_rho_sky -= (dposx * dt / 10)

        # Setting up the view transform
        if controller.follow_player:
            R = controller.zoom
            # Coordenadas cilindricas
            # camX = R*np.sin(controller.camera_theta)
            # camY = R*np.cos(controller.camera_theta)
            # camZ = camera_z *-30

            # Coordenadas esfericas
            camX = np.sin(controller.camera_theta) * np.cos(controller.camera_rho)
            camY = np.sin(controller.camera_theta) * np.sin(controller.camera_rho)
            camZ = np.cos(controller.camera_theta)
            viewPos = np.array([camX, camY, camZ])
            viewPosNor = viewPos / np.linalg.norm(viewPos)
            normal_view = tr.lookAt(
                np.array([controller.x, controller.y, 0]),
                viewPosNor + np.array([controller.x, controller.y, 0]),
                np.array([0, 0, 1])
            )

        else:
            # static camera
            normal_view = tr.lookAt(
                np.array([6 + controller.x, 6 + controller.y, 8]),
                np.array([0 + controller.x, 0 + controller.y, 0]),
                np.array([0, 0, 1])
            )
        camX2 = np.cos(controller.camera_theta_sky) * np.sin(-controller.camera_rho_sky)
        camY2 = np.sin(controller.camera_theta_sky)
        camZ2 = np.cos(controller.camera_theta_sky) * np.cos(controller.camera_rho_sky)
        viewPos2 = np.array([camZ2, camY2, camX2])
        viewPos2nor = viewPos2 / np.linalg.norm(viewPos2)
        normal_view2 = tr.lookAt(
            np.array([controller.x, controller.y, controller.z]),
            viewPos2nor + np.array([controller.x, controller.y, controller.z]),
            np.array([0, 1, 0])
        )

        view = np.copy(normal_view2)

        for i in range(3):
            view[i][3] = 0

        if controller.night:
            cubeMapTexture = cubeMapTextureNight
        else:
            cubeMapTexture = cubeMapTextureDay

        # Render SkyBox
        glDepthMask(GL_FALSE)

        glDisable(GL_DEPTH_TEST)
        glUseProgram(TextureShaderSky.shaderProgram)
        glActiveTexture(GL_TEXTURE0)
        glUniformMatrix4fv(glGetUniformLocation(TextureShaderSky.shaderProgram, "view"), 1, GL_TRUE, view)
        glUniformMatrix4fv(glGetUniformLocation(TextureShaderSky.shaderProgram, "projection"), 1, GL_TRUE, projection)

        # skybox cube
        glBindVertexArray(skyboxVAO)
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture)
        glDrawArrays(GL_TRIANGLES, 0, 36)
        glBindVertexArray(0)
        glEnable(GL_DEPTH_TEST)
        glDepthMask(GL_TRUE)

        norm = np.sqrt(camX * camX + camY * camY)
        sumaX = camX / norm * dt * controller.veloc
        sumaY = camY / norm * dt * controller.veloc

        if glfw.get_key(window, glfw.KEY_W) == glfw.PRESS:

            if (checkClose(controller.collisions, [controller.x + sumaX, controller.y + sumaY],
                           controller.tolerance) == False or controller.noclip == True):
                vector = np.array([controller.x, controller.y])
                controller.x += sumaX
                controller.y += sumaY
                # controller.z+= (camZ / R * dt * controller.veloc)
        elif glfw.get_key(window, glfw.KEY_S) == glfw.PRESS:
            if (checkClose(controller.collisions, [controller.x - sumaX, controller.y - sumaY],
                           controller.tolerance) == False or controller.noclip == True):
                controller.x -= sumaX
                controller.y -= sumaY

        elif glfw.get_key(window, glfw.KEY_A) == glfw.PRESS:
            if (checkClose(controller.collisions, [controller.x - sumaY, controller.y + sumaX],
                           controller.tolerance) == False or controller.noclip == True):
                controller.x -= sumaY
                controller.y += sumaX

            # else:
        elif glfw.get_key(window, glfw.KEY_D) == glfw.PRESS:
            if (checkClose(controller.collisions, [controller.x + sumaY, controller.y - sumaX],
                           controller.tolerance) == False or controller.noclip == True):
                controller.x += sumaY
                controller.y -= sumaX

        if checkClose(controller.treasureColl, [controller.x, controller.y], controller.treasureClose):
            num = getCol(controller.treasureColl, [controller.x, controller.y], controller.treasureClose)

            for i in range(len(controller.treasure)):

                trans = sg.findTransform(controller.treasure[i], "treasure")
                x = trans[0][3]
                y = trans[1][3]

                if np.abs(num[0] - x) <= 0.5 and np.abs(num[1] - y) <= 0.5:
                    controller.treasure = np.delete(controller.treasure, i)
                    if len(controller.treasure) == 0:
                        tf = glfw.get_time()
                        print("CONGRATULATIONS!!!")
                        t = tf - ti
                        if t >= 60:
                            entd = t / 60
                            ent = int(entd)
                            dec = entd - ent
                            seg = 60 * dec

                            print(
                                "You completed the maze in " + str(ent) + " minutes and " + str(int(seg)) + " seconds")
                        else:
                            print("You completed the maze in " + str(int(t)) + " seconds")

                        sys.exit()
                    break

        glfw.set_scroll_callback(window, scroll_callback)

        glUseProgram(TextureShader.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "view"), 1, GL_TRUE, normal_view)
        glUniformMatrix4fv(glGetUniformLocation(TextureShader.shaderProgram, "model"), 1, GL_TRUE, model)

        glUseProgram(phongPipeline.shaderProgram)

        if controller.night:
            # White light in all components: ambient, diffuse and specular.
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "La"), 0, 0, 0)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1, 1, 1)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1, 1, 1)

            # Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), 0, 0, 0)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), 0.7, 0.5, 0.4)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), 0.8, 0.6, 0.5)

            # TO DO: Explore different parameter combinations to understand their effect!

            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), controller.x, controller.y,
                        0.5)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), controller.x, controller.y,
                        0)
            glUniform1ui(glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), 20)

            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"), controller.constant)
            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), controller.lineal)
            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"), controller.quadratic)

            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)
            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "model"), 1, GL_TRUE, model)

        else:
            # White light in all components: ambient, diffuse and specular.
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "La"), 1, 1, 1)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1, 1, 1)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1, 1, 1)

            # Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), 1, 1, 1)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), 1, 1, 1)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), 1, 1, 1)

            # TO DO: Explore different parameter combinations to understand their effect!

            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), controller.x, controller.y,
                        80)
            glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), controller.x, controller.y,
                        0)
            glUniform1ui(glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), 0)

            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"), 1)
            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), 1)
            glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"), 1)

            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)
            glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "model"), 1, GL_TRUE, model)

        for i in controller.treasure:
            sg.drawSceneGraphNode(i, phongPipeline, "model")

        for i in controller.walls:
            sg.drawSceneGraphNode(i, phongPipeline, "model")

        # Swap the buffers
        glfw.swap_buffers(window)

    glfw.terminate()
