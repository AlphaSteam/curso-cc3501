import numpy as np


grid = np.zeros((20,15), dtype=int)

# maze borders
grid[0,:] = 1
grid[19,:] = 1
grid[:,0] = 1
grid[:,14] = 1

# maze inner walls
#grid[3:8,5:7] = 1
grid[1:8,2]=1
grid[7,2:20]=1
grid[9:18,2]=1
grid[9,2:20]=1
grid[2,4:]=1
grid[2,7]=0
grid[2:5,6]=1
grid[2:5,8]=1
grid[3:5,4]=1
grid[2,5]=0
grid[5,8:12]=1
grid[6:,12]=1
grid[2,13]=0
grid[15,7:9]=1
grid[16,7:9]=1
# treasures!

grid[8,1] = 3
grid[10,6] = 3
grid[1,2]=3
grid[3,3]=3
grid[16,10]=3

# player start
grid[1,1] = 2
print(grid)





np.save('maze.npy', grid)

